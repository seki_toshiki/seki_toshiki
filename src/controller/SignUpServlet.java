package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet (urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();

    	List<Branch> allBranches = new BranchService().getBranch();	//支店一覧
        session.setAttribute("allBranches", allBranches);

        List<Department> allDepartments = new DepartmentService().getDepartment();	//役職一覧
        session.setAttribute("allDepartments", allDepartments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

//        HttpSession session = request.getSession();

        User user = new User();

        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        user.setDepart_id(Integer.parseInt(request.getParameter("depart_id")));

        if (isValid(request, messages) == true) {

//            User user = new User();
//
//            user.setAccount(request.getParameter("account"));
//            user.setPassword(request.getParameter("password"));
//            user.setName(request.getParameter("name"));
//            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
//            user.setDepart_id(Integer.parseInt(request.getParameter("depart_id")));

            new UserService().register(user);

            response.sendRedirect("manegement");
        } else {
        	request.setAttribute("tempUser", user);
        	request.setAttribute("errorMessages", messages);
            //response.sendRedirect("signup");
        	request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String branch = request.getParameter("branch_id");
        String depart = request.getParameter("depart_id");
        User checkDuplication = UserService.CheckDuplication(account);

        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        } else if (checkDuplication != null) {
        } else if (Pattern.matches("^[0-9a-zA-Z]+$", account) == false) {
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下で設定してください");
        } else if (account.length() <= 5) {
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下で設定してください");
        }

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }

        if (StringUtils.isEmpty(password2) == true) {
            messages.add("パスワード(確認)を入力してください");
        } else if (password.length() <= 5 || password.length() >= 21) {
            messages.add("パスワードは6文字以上20文字以下で設定してください");
        } else if( !((password.getBytes().length) == password.length())) {
        	messages.add("パスワードは半角文字で設定してください");
        } else if( StringUtils.isNotEmpty(password) == true && StringUtils.isNotEmpty(password2) == true && password.equals(password2) == false) {
        	messages.add("パスワードが一致しません。パスワードを確認してください");
        }


        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        } else if (name.length() > 10) {
        	 messages.add("名前は10文字以内で入力してください");
        }

        if (StringUtils.isEmpty(branch) == true) {
            messages.add("支店を入力してください");
        }
        if (StringUtils.isEmpty(depart) == true) {
            messages.add("部署・役職を入力してください");
        }
        if (checkDuplication != null) {
        	messages.add("アカウント名が既に使用されています");
        }

        if ( depart.equals("1") || depart.equals("2")) {
        	if (!(branch.equals("4"))) {
        		messages.add("支店と部署の組み合わせが無効です");
        	}
        } else {
        	if (branch.equals("4")){
        		messages.add("支店と部署の組み合わせが無効です");
        	}
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}