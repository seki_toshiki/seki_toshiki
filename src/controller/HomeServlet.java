package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Contribution;
//import beans.User;
import service.CommentService;
import service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet { private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

    	//検索条件を取得
    	String start = request.getParameter("start");
    	String end = request.getParameter("end");
    	String searchCategory = request.getParameter("searchCategory");
    	request.setAttribute("tempStart", start);
    	request.setAttribute("tempEnd", end);
    	request.setAttribute("tempSearchCategory", searchCategory);

    	//投稿一覧
        List<Contribution> contributions = new ContributionService().getAllContribution(start, end, searchCategory);
        request.setAttribute("contributions", contributions);

        //コメント一覧
        List<Comment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

    	//検索条件を取得
    	String start = request.getParameter("start");
    	String end = request.getParameter("end");
    	String searchCategory = request.getParameter("searchCategory");

    	//投稿一覧
        List<Contribution> contributions = new ContributionService().getAllContribution(start, end, searchCategory);
        request.setAttribute("contributions", contributions);

        //コメント一覧
        List<Comment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}