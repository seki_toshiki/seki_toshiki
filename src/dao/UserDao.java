package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	//新規登録
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("id");
            sql.append(", account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", depart_id");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // depart_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, user.getId());
            ps.setString(2, user.getAccount());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getName());
            ps.setInt(5, user.getBranch_id());
            ps.setInt(6, user.getDepart_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ログイン
    public User getUser(Connection connection, String account, String password ) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int depart_id = rs.getInt("depart_id");
                int is_banned = rs.getInt("is_banned");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setDepart_id(depart_id);
                user.setIs_banned(is_banned);
                user.setCreated_date(createdDate);
                user.setUpdated_date(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //停止復活
    public void ban(Connection connection, User user) {

        PreparedStatement ps = null;

        try {
        	StringBuilder sql = new StringBuilder();
        	sql.append("UPDATE users SET");
        	sql.append(" is_banned = ?");
        	sql.append(", updated_date = CURRENT_TIMESTAMP");
        	sql.append(" WHERE");
        	sql.append(" id = ?");

        	ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, user.getIs_banned());
            ps.setInt(2, user.getId());

            ps.executeUpdate();


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    //ユーザー編集
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;

        try {
            if (!((user.getPassword()).isEmpty())) {
            	StringBuilder sql = new StringBuilder();
            	sql.append("UPDATE users SET");
            	sql.append(" account = ?");
            	sql.append(", password = ?");
            	sql.append(", name = ?");
            	sql.append(", branch_id = ?");
            	sql.append(", depart_id = ?");
            	sql.append(", is_banned = ?");
            	sql.append(", updated_date = CURRENT_TIMESTAMP");
            	sql.append(" WHERE");
            	sql.append(" id = ?");

            	ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, user.getAccount());
	            ps.setString(2, user.getPassword());
	            ps.setString(3, user.getName());
	            ps.setInt(4, user.getBranch_id());
	            ps.setInt(5, user.getDepart_id());
	            ps.setInt(6, user.getIs_banned());
	            ps.setInt(7, user.getId());

	            ps.executeUpdate();

            } else {
            	StringBuilder sql = new StringBuilder();
            	sql.append("UPDATE users SET");
            	sql.append(" account = ?");
            	sql.append(", name = ?");
            	sql.append(", branch_id = ?");
            	sql.append(", depart_id = ?");
            	sql.append(", is_banned = ?");
            	sql.append(", updated_date = CURRENT_TIMESTAMP");
            	sql.append(" WHERE");
            	sql.append(" id = ?");

            	ps = connection.prepareStatement(sql.toString());

            	ps.setString(1, user.getAccount());
	            ps.setString(2, user.getName());
	            ps.setInt(3, user.getBranch_id());
	            ps.setInt(4, user.getDepart_id());
	            ps.setInt(5, user.getIs_banned());
	            ps.setInt(6, user.getId());

	            ps.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    //アカウントが重複していないかチェック
    public User CheckDuplication(Connection connection, String account) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);

            System.out.println(account);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
