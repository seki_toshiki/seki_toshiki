package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.GetUserDao;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public User getEditUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



    public List<User> getAllUser() {

        Connection connection = null;
        try {
            connection = getConnection();

            GetUserDao getUserDao = new GetUserDao();
            List<User> ret = getUserDao.getUserList(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //重複チェック
    public static User CheckDuplication(String account) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.CheckDuplication(connection, account);

            commit(connection);

            if(user == null) {
            	return null;
            } else {
            	return user;
            }


        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}