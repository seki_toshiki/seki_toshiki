<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript"><!--
			function deletePost(){
			// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	        var res = confirm("投稿を削除します。よろしいですか？");
	        if (res == true) {
	             return true;
	        }
	        // 「OK」時の処理終了
	        // 「キャンセル」時の処理開始
	        else {
	            window.alert('キャンセルされました'); // 警告ダイアログを表示
	            return false;
	        }
	        // 「キャンセル」時の処理終了
			}
		// --></script>

		<script type="text/javascript"><!--
			function deleteComment(){
			// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	        var res = confirm("コメントを削除します。よろしいですか？");
	        if (res == true) {
	             return true;
	        }
	        // 「OK」時の処理終了
	        // 「キャンセル」時の処理開始
	        else {
	            window.alert('キャンセルされました'); // 警告ダイアログを表示
	            return false;
	        }
	        // 「キャンセル」時の処理終了
			}
		// --></script>

	</head>
	<body>
		<div class ="main-conntents">
			<div class="header">

				<c:if test="${ not empty loginUser }"><br>
        			<div class="name">へいらっしゃい！<b><c:out value="${loginUser.name}" /></b>さん</div>
            		<input type="hidden" name="depart_Id" value="${loginUser.depart_id}">&ensp;&ensp;
				</c:if>

   		 		<c:if test="${ not empty loginUser }">
        			<a href="./">ホーム</a>
        			<a href="contribution">新規投稿</a>
        		<c:if test="${ not empty Author }">
        			<a href="manegement">ユーザー管理</a>
        		</c:if>
        			<a href="logout">ログアウト</a><br>
    			</c:if>
				&ensp;&ensp;

			</div>



			<c:if test="${ not empty authorityErrors }">
                <div class="error">
                    <c:forEach items="${authorityErrors}" var="authorityError">
                        <li><c:out value="${authorityError}" /><br />
                    </c:forEach>
                </div>
                <c:remove var="authorityErrors" scope="session"/>
            </c:if>

            <c:if test="${ not empty commentErrorMessages }">
                <div class="error">
                    <c:forEach items="${commentErrorMessages}" var="commentErrorMessage">
                        <li><c:out value="${commentErrorMessage}" /><br />
                    </c:forEach>
                </div>
                <c:remove var="commentErrorMessages" scope="session"/>
            </c:if>

				<div class="contribution">

				<div class="kugiri"></div>

					<div class="serchContribution">
						<form action="./" method="get">
						投稿検索&ensp;&ensp;期間：
						<c:choose>
							<c:when test="${ empty tempStart}">
								<input type="date" id="start" name="start" min="2019-03-01">
							</c:when>
							<c:when test="${ not empty tempStart}">
								<input type="date" id="start" name="start" min="2019-03-01" value="${tempStart}">
							</c:when>
						</c:choose>
						～
						<c:choose>
							<c:when test="${ empty tempEnd}">
								<input type="date" id="end" name="end" min="2019-03-01">
							</c:when>
							<c:when test="${ not empty tempEnd}">
								<input type="date" id="end" name="end" min="2019-03-01" value="${tempEnd}">
							</c:when>
						</c:choose>
						&ensp;カテゴリ：
						<c:choose>
							<c:when test="${ empty tempSearchCategory}">
								<input id="searchCategory" name="searchCategory">
							</c:when>
							<c:when test="${ not empty tempSearchCategory}">
								<input id="searchCategory" name="searchCategory" value="${tempSearchCategory}">
							</c:when>
						</c:choose>
						<input type="submit" value="🔍" /><br>
						</form>
					</div>

				<br><hr noshade width="85%"><br>

				<c:if test="${ empty contributions }">
				<h4>検索条件にヒットする投稿はありませんでした</h4>
				</c:if>

    			<c:forEach items="${contributions}" var="contribution">

            		<div class="message">

                	<div class="name-subject-category">

                    <h2><span class="subject"><b><c:out value="${contribution.subject}" /></b></span></h2>
                    <div class="date"><fmt:formatDate value="${contribution.created_date}" pattern="yyyy/MM/dd(E) HH:mm" /></div><br>
					<span class="category">カテゴリ：<c:out value="${contribution.category}" /></span>
					<span class="name">&ensp;&ensp;&ensp;&ensp;投稿者：<c:out value="${contribution.name}" /></span><br>

                	</div>
                	<br><div class="contribution-text"><pre><c:out value="${contribution.text}" /></pre></div>
            		</div>

            		<div class="deleteContribution">
           				<c:if test= "${contribution.user_id == loginUser.id}">
           					<form action="deleteContribution" method="post">
           					<input type="submit" value="投稿削除" onclick="return deletePost()"/>
	                		<input type="hidden" name="contribution_id" value="${contribution.id}">
           					</form>
           				</c:if>
         			</div>

            		<div class="comment">
            			<br><b><span class="comment"><c:out value="コメント" /></span></b>
            			<hr width="20%" align="left">

                		<c:forEach items="${comments}" var="comment">
                			<div class="showComment">
                				<c:if test= "${comment.post_id == contribution.id}">
                					<p><c:out value="${comment.name}" />
                					&ensp;&ensp;<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd(E) HH:mm" />
                					<div class="comment-text"><pre><c:out value="${comment.text}" /></pre></div>

	                				<c:if test= "${comment.user_id == loginUser.id}">
	                					<form action="deleteComment" method="post">
	                					<input type="submit" value="コメント削除" onclick="return deleteComment()"/>
							            <input type="hidden" name="comment_id" value="${comment.id}">
	                					</form>
	                				</c:if>
                				</c:if>
                			</div>
                		</c:forEach>

                		<form action="comment" method="post">
                		<br>
	            			<textarea name="text" id="text" rows="4" cols="140"></textarea>
			                <input type="submit" value="コメント" /><br />
			                <input type="hidden" name="user_id" value="${loginUser.id}">
			                <input type="hidden" name="post_id" value="${contribution.id}">
                		</form>



                		<br><hr noshade width="85%"><br>

            		</div>
    			</c:forEach><br><br>
			</div>	<br><div class ="copyright">Copyright(c) Toshiki Seki</div>
		</div>
	</body>
</html>