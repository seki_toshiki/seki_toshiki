<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        	<div class = "header">
				<a href="./">ホーム</a>
				<a href="manegement">戻る</a>
			</div>

            <c:if test="${ not empty errorMessages }">
                <div class="error">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br />

				<div class="editUser">

                <c:if test="${ not empty tempUser }">

                	<div class="midashi">ログインID</div>  <br /><br /><input name="account" id="account" value="${tempUser.account}"/> <br /><br />
                	<div class="midashi">パスワードa</div>  <br /><br /><input name="password" type="password" id="password" /> <br /><br />
                	<div class="midashi">パスワード(確認)</div>  <br /><br /><input name="password2" type="password" id="password2" /> <br /><br />
                	<div class="midashi">名前</div>  <br /><br /><input name="name" id="name" value="${tempUser.name}"/> <br /><br />

	                <div class="midashi">支店</div> <br /><br />
	                	<select name="branch_id">
							<c:forEach var="branch" items="${allBranches}">
	              				<c:if test= "${ tempUser.branch_id == branch.id}">
	              					<option value="${branch.id}" selected><c:out value="${branch.name}" /></option>
	              				</c:if>
	              			</c:forEach>
	              			<c:forEach var="branch" items="${allBranches}">
	              				<c:if test= "${ tempUser.branch_id != branch.id }">
	              					<option value="${branch.id}"><c:out value="${branch.name}" /></option>
	              				</c:if>
							</c:forEach>
						</select><br><br />

	                <div class="midashi">部署・役職</div> <br /><br />
	                	<select name="depart_id">
							<c:forEach var="depart" items="${allDepartments}">
	              				<c:if test= "${ tempUser.depart_id == depart.id}">
	              					<option value="${depart.id}" selected><c:out value="${depart.name}" /></option>
	              				</c:if>
	              			</c:forEach>
	              			<c:forEach var="depart" items="${allDepartments}">
	              				<c:if test= "${ tempUser.depart_id != depart.id }">
	              					<option value="${depart.id}"><c:out value="${depart.name}" /></option>
	              				</c:if>
							</c:forEach>
						</select><br><br />

                </c:if>



                <c:if test="${  empty tempUser }">

                	<div class="midashi">ログインID</div> <br /><br /><input name="account" id="account" /> <br /><br />
                	<div class="midashi">パスワード</div> <br /><br /><input name="password" type="password" id="password" /> <br /><br />
                	<div class="midashi">パスワード(確認)</div>  <br /><br /><input name="password2" type="password" id="password2" /> <br /><br />
                	<div class="midashi">名前</div> <br /><br /><input name="name" id="name" /> <br /><br />

                	<div class="midashi">支店</div><br /><br />
		               	<select name="branch_id">
							<c:forEach var="branch" items="${allBranches}">
							<option value="${branch.id}"><c:out value="${branch.name}" /></option>
							</c:forEach>
						</select><br><br />

	                <div class="midashi">部署・役職</div><br /><br />
	                	<select name="depart_id">
							<c:forEach var="depart" items="${allDepartments}">
							<option value="${depart.id}"><c:out value="${depart.name}" /></option>
							</c:forEach>
						</select><br><br />

                </c:if>

                <br /> <input type="submit" value="登録" /> <br /><br />
                </div>
                <a href="manegement">戻る</a><br />
            </form>
            <br><div class="copyright">Copyright(c) Toshiki Seki</div>
        </div>
    </body>
</html>